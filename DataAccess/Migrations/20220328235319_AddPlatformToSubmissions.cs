﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace DataAccess.Migrations
{
    public partial class AddPlatformToSubmissions : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "PlatformId",
                schema: "Competition",
                table: "CompetitionSubmission",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_CompetitionSubmission_PlatformId",
                schema: "Competition",
                table: "CompetitionSubmission",
                column: "PlatformId");

            migrationBuilder.AddForeignKey(
                name: "FK_CompetitionSubmission_CompetitionPlatform_PlatformId",
                schema: "Competition",
                table: "CompetitionSubmission",
                column: "PlatformId",
                principalSchema: "Competition",
                principalTable: "CompetitionPlatform",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_CompetitionSubmission_CompetitionPlatform_PlatformId",
                schema: "Competition",
                table: "CompetitionSubmission");

            migrationBuilder.DropIndex(
                name: "IX_CompetitionSubmission_PlatformId",
                schema: "Competition",
                table: "CompetitionSubmission");

            migrationBuilder.DropColumn(
                name: "PlatformId",
                schema: "Competition",
                table: "CompetitionSubmission");
        }
    }
}
