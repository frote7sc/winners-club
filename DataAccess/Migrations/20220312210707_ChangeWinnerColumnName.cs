﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace DataAccess.Migrations
{
    public partial class ChangeWinnerColumnName : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "WinnerDiscordId",
                schema: "WinnersClub",
                table: "Contract",
                newName: "Winner");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Winner",
                schema: "WinnersClub",
                table: "Contract",
                newName: "WinnerDiscordId");
        }
    }
}
