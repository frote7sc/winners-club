﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace DataAccess.Migrations
{
    public partial class CreateCompetitionSubmissionTables : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "CompetitionSubmission",
                schema: "Competition",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CompetitionId = table.Column<int>(type: "int", nullable: false),
                    DurationInSeconds = table.Column<int>(type: "int", nullable: false),
                    Score = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CompetitionSubmission", x => x.Id);
                    table.ForeignKey(
                        name: "FK_CompetitionSubmission_Competition_CompetitionId",
                        column: x => x.CompetitionId,
                        principalSchema: "Competition",
                        principalTable: "Competition",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "CompetitionSubmissionVideo",
                schema: "Competition",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    SubmissionId = table.Column<int>(type: "int", nullable: false),
                    VideoUrl = table.Column<string>(type: "nvarchar(max)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CompetitionSubmissionVideo", x => x.Id);
                    table.ForeignKey(
                        name: "FK_CompetitionSubmissionVideo_CompetitionSubmission_SubmissionId",
                        column: x => x.SubmissionId,
                        principalSchema: "Competition",
                        principalTable: "CompetitionSubmission",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_CompetitionSubmission_CompetitionId",
                schema: "Competition",
                table: "CompetitionSubmission",
                column: "CompetitionId");

            migrationBuilder.CreateIndex(
                name: "IX_CompetitionSubmissionVideo_SubmissionId",
                schema: "Competition",
                table: "CompetitionSubmissionVideo",
                column: "SubmissionId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "CompetitionSubmissionVideo",
                schema: "Competition");

            migrationBuilder.DropTable(
                name: "CompetitionSubmission",
                schema: "Competition");
        }
    }
}
