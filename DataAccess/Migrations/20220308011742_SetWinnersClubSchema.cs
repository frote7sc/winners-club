﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace DataAccess.Migrations
{
    public partial class SetWinnersClubSchema : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.EnsureSchema(
                name: "WinnersClub");

            migrationBuilder.RenameTable(
                name: "PlatformToPublicId",
                newName: "PlatformToPublicId",
                newSchema: "WinnersClub");

            migrationBuilder.RenameTable(
                name: "ContractCollection",
                newName: "ContractCollection",
                newSchema: "WinnersClub");

            migrationBuilder.RenameTable(
                name: "Contract",
                newName: "Contract",
                newSchema: "WinnersClub");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameTable(
                name: "PlatformToPublicId",
                schema: "WinnersClub",
                newName: "PlatformToPublicId");

            migrationBuilder.RenameTable(
                name: "ContractCollection",
                schema: "WinnersClub",
                newName: "ContractCollection");

            migrationBuilder.RenameTable(
                name: "Contract",
                schema: "WinnersClub",
                newName: "Contract");
        }
    }
}
