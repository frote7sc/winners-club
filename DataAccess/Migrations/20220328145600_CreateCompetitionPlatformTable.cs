﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace DataAccess.Migrations
{
    public partial class CreateCompetitionPlatformTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "CompetitionPlatform",
                schema: "Competition",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Platform = table.Column<string>(type: "nvarchar(max)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CompetitionPlatform", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "CompetitionCompetitionPlatform",
                schema: "Competition",
                columns: table => new
                {
                    CompetitionsId = table.Column<int>(type: "int", nullable: false),
                    PlatformsId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CompetitionCompetitionPlatform", x => new { x.CompetitionsId, x.PlatformsId });
                    table.ForeignKey(
                        name: "FK_CompetitionCompetitionPlatform_Competition_CompetitionsId",
                        column: x => x.CompetitionsId,
                        principalSchema: "Competition",
                        principalTable: "Competition",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_CompetitionCompetitionPlatform_CompetitionPlatform_PlatformsId",
                        column: x => x.PlatformsId,
                        principalSchema: "Competition",
                        principalTable: "CompetitionPlatform",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_CompetitionCompetitionPlatform_PlatformsId",
                schema: "Competition",
                table: "CompetitionCompetitionPlatform",
                column: "PlatformsId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "CompetitionCompetitionPlatform",
                schema: "Competition");

            migrationBuilder.DropTable(
                name: "CompetitionPlatform",
                schema: "Competition");
        }
    }
}
