namespace DataAccess.Models;

public class PlatformToPublicId
{
    public int Id { get; set; }
    public string Platform { get; set; }
    public string PublicId { get; set; }
    public Contract Contract { get; set; }
}