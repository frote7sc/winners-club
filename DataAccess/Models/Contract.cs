namespace DataAccess.Models;

public class Contract
{
    public int Id { get; set; }
    public string Event { get; set; }
    public string Winner { get; set; }
    public List<PlatformToPublicId> ContractIds { get; set; }
    public ContractCollection ContractCollection { get; set; }
}