namespace DataAccess.Models;

public class ShortLink
{
    public int Id { get; set; }
    public string Key { get; set; } = null!;
    public string RedirectUrl { get; set; } = null!;
    public int Uses { get; set; } = 0;
}