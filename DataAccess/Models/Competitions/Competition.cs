namespace DataAccess.Models.Competitions;

public class Competition
{
    public int Id { get; set; }
    public string Name { get; set; } = null!;
    public string? Description { get; set; }
    public DateTime StartTime { get; set; }
    public DateTime EndTime { get; set; }
    public List<CompetitionPlatform> Platforms { get; set; }
    public List<CompetitionSubmission> Submissions { get; set; }
}