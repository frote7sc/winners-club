namespace DataAccess.Models.Competitions;

public class CompetitionPlatform
{
    public int Id { get; set; }
    public string Platform { get; set; }
    public List<Competition> Competitions { get; set; }
}