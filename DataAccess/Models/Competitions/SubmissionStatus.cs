namespace DataAccess.Models.Competitions;

public enum SubmissionStatus
{
    Pending = 0,
    Verified = 1,
    Rejected = 2
}