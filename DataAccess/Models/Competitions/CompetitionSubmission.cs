namespace DataAccess.Models.Competitions;

public class CompetitionSubmission
{
    public int Id { get; set; }
    public Competition Competition { get; set; } = null!;
    public CompetitionPlatform Platform { get; set; } = null!;
    public string DiscordId { get; set; } = null!;
    public int DurationInSeconds { get; set; }
    public int Score { get; set; }
    public SubmissionStatus Status { get; set; }
    public List<CompetitionSubmissionVideo> Videos { get; set; } = new();
    
    public Guid Fingerprint { get; set; }
}