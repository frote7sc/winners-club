namespace DataAccess.Models.Competitions;

public class CompetitionSubmissionVideo
{
    public int Id { get; set; }
    public CompetitionSubmission Submission { get; set; }
    public string VideoUrl { get; set; }
}