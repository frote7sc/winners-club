namespace DataAccess.Models;

public class ContractCollection
{
    public int Id { get; set; }
    public int Month { get; set; }
    public int Year { get; set; }
    public List<Contract> Contracts { get; set; }
}