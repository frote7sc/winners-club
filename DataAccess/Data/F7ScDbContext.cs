using DataAccess.Models;
using DataAccess.Models.Competitions;
using Microsoft.EntityFrameworkCore;

namespace DataAccess.Data;

public class F7ScDbContext : DbContext
{
    public F7ScDbContext(DbContextOptions<F7ScDbContext> options) : base(options)
    {
        
    }

    public DbSet<Contract> Contracts { get; set; }
    public DbSet<ContractCollection> ContractCollections { get; set; }
    public DbSet<CompetitionPlatform> CompetitionPlatforms { get; set; }
    public DbSet<Competition> Competitions { get; set; }
    public DbSet<CompetitionSubmission> Submissions { get; set; }
    public DbSet<ShortLink> ShortLinks { get; set; }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        // Competition
        modelBuilder.Entity<Competition>().ToTable("Competition", "Competition");
        modelBuilder.Entity<CompetitionPlatform>().ToTable("CompetitionPlatform", "Competition");
        modelBuilder.Entity<CompetitionSubmission>().ToTable("CompetitionSubmission", "Competition");
        modelBuilder.Entity<CompetitionSubmissionVideo>().ToTable("CompetitionSubmissionVideo", "Competition");
        
        // Winner's Club
        modelBuilder.Entity<PlatformToPublicId>().ToTable("PlatformToPublicId", "WinnersClub");
        modelBuilder.Entity<Contract>().ToTable("Contract", "WinnersClub");
        modelBuilder.Entity<ContractCollection>().ToTable("ContractCollection", "WinnersClub");
        
        // Short Links
        modelBuilder.Entity<ShortLink>().ToTable("ShortLink", "ShortLink")
            .HasIndex(x => x.Key).IsUnique();
    }
}