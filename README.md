# F7SC.com APIs

APIs for calling F7SC.com services

## Creating a New Migration
Inside of the `DataAccess` folder, execute the following:

```
dotnet ef migrations add MigrationName --startup-project ../API
```

## Applying Migrations
Inside of the `DataAccess` folder, execute the following:

```
dotnet ef database update --startup-project ../API
```
