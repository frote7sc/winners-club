using DataAccess.Models;
using F7SCAPI.BusinessLogic;
using F7SCAPI.BusinessLogic.Discord;
using F7SCAPI.ViewModels;
using Microsoft.AspNetCore.Mvc;

namespace F7SCAPI.Controllers;

public class WinnersClubController : ApiControllerBase
{
    private readonly IWinnersClubService _winnersClubService;

    public WinnersClubController(IWinnersClubService winnersClubService)
    {
        _winnersClubService = winnersClubService;
    }

    [DiscordAuthRequired(F7ScRoles.Admin)]
    [HttpPost("collections")]
    public IActionResult UpsertCollection(ContractCollectionViewModel viewModel)
    {
        if (!IsMonthAndYearValid(viewModel.Month, viewModel.Year))
        {
            return BadRequest(new
            {
                Message = $"Month ({viewModel.Month}) and Year ({viewModel.Year}) must be valid"
            });
        }
        
        _winnersClubService.UpsertWinnersClubBatch(viewModel);
        return NoContent();
    }

    private static bool IsMonthAndYearValid(int month, int year)
    {
        try
        {
            var _ = new DateTime(year, month, 1);
            return true;
        }
        catch (ArgumentOutOfRangeException)
        {
            return false;
        }
    }

    [HttpGet("collections")]
    public IActionResult GetCollection(int month, int year)
    {
        if (!IsMonthAndYearValid(month, year))
        {
            return BadRequest(new
            {
                Message = $"Month ({month}) and Year ({year}) must be valid"
            });
        }
        
        var result = _winnersClubService.GetWinnersClubBatch(month, year);
        return result != null ? Ok(TransformContractCollection(result)) : NotFound(new
        {
            Message = $"No contracts found for month {month} and year {year}"
        });
    }

    private static ContractCollectionViewModel TransformContractCollection(ContractCollection collection)
    {
        return new ContractCollectionViewModel
        {
            Month = collection.Month,
            Year = collection.Year,
            Contracts = collection.Contracts.Select(contract => new ContractViewModel
            {
                Event = contract.Event,
                Winner = contract.Winner,
                ContractIds = contract.ContractIds.Select(id => new PlatformToPublicIdViewModel
                {
                    Platform = id.Platform,
                    PublicId = id.PublicId
                }).ToList()
            }).ToList()
        };
    }
}