using DataAccess.Models.Competitions;
using F7SCAPI.BusinessLogic;
using F7SCAPI.BusinessLogic.Discord;
using F7SCAPI.ViewModels.Competitions;
using F7SCAPI.ViewModels.Submissions;
using Microsoft.AspNetCore.Mvc;

namespace F7SCAPI.Controllers;

public class SubmissionController : ApiControllerBase
{
    private readonly IDiscordService _discordService;
    private readonly ISubmissionService _submissionService;

    public SubmissionController(IDiscordService discordService, ISubmissionService submissionService)
    {
        _discordService = discordService;
        _submissionService = submissionService;
    }

    [HttpPost("competitions/{competitionId}/submissions")]
    public async Task<IActionResult> Submit([FromHeader(Name = "x-discord-token")] string discordToken, int competitionId, CreateSubmissionViewModel viewModel)
    {
        var discordId = await GetDiscordIdFromTokenAsync(discordToken);
        if (discordId == null)
        {
            return BadRequest(new
            {
                Message = "Discord token invalid or malformed."
            });
        }

        var submission = _submissionService.Submit(discordId!, competitionId, viewModel);

        return Ok(new SubmissionViewModel
        {
            Id = submission.Id,
            Platform = submission.Platform.Platform,
            DiscordName = "N/A",
            DurationInSeconds = submission.DurationInSeconds,
            Score = submission.Score,
            Status = Enum.GetName(typeof(SubmissionStatus), submission.Status)!,
            VideoUrls = submission.Videos.Select(x => x.VideoUrl).ToList()
        });
    }

    private async Task<string?> GetDiscordIdFromTokenAsync(string token)
    {
        var splitDiscordToken = token.Split(' ');
        if (splitDiscordToken.Length != 2)
        {
            return null;
        }

        return await _discordService.VerifyTokenAsync(splitDiscordToken[0], splitDiscordToken[1]);
    }

    [DiscordAuthRequired(F7ScRoles.Admin)]
    [HttpPatch("competitions/{competitionId}/submissions/{submissionId}")]
    public IActionResult HandleSubmission(int competitionId, int submissionId,
        [FromBody] HandleSubmissionViewModel patchModel)
    {
        try
        {
            var newFingerprint = _submissionService.HandleSubmission(competitionId, submissionId, patchModel);

            return Ok(new
            {
                NewFingerprint = newFingerprint 
            });
        }
        catch (FingerprintViolationException)
        {
            return Conflict(new
            {
                message = "Fingerprint provided does not match database's fingerprint."
            });
        }
    }

    [HttpGet("submissions/@me")]
    public async Task<IActionResult> FetchMySubmissions([FromHeader(Name = "x-discord-token")] string discordToken)
    {
        var discordId = await GetDiscordIdFromTokenAsync(discordToken);
        if (discordId == null)
        {
            return BadRequest(new
            {
                Message = "Discord token invalid or malformed."
            });
        }

        var submissions = _submissionService.GetMySubmissions(discordId);

        return Ok(submissions.Select(x => new MySubmissionViewModel
        {
            Id = x.Id,
            CompetitionId = x.Competition.Id,
            CompetitionName = x.Competition.Name,
            Platform = x.Platform.Platform,
            DurationInSeconds = x.DurationInSeconds,
            Score = x.Score,
            Status = Enum.GetName(typeof(SubmissionStatus), x.Status)!,
            VideoUrls = x.Videos.Select(y => y.VideoUrl).ToList()
        }).ToList());
    }
}