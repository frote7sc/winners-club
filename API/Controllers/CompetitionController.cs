using DataAccess.Models.Competitions;
using F7SCAPI.BusinessLogic;
using F7SCAPI.BusinessLogic.Discord;
using F7SCAPI.ViewModels.Competitions;
using F7SCAPI.ViewModels.Submissions;
using Microsoft.AspNetCore.Mvc;

namespace F7SCAPI.Controllers;

public class CompetitionController : ApiControllerBase
{
    private readonly ICompetitionService _competitionService;
    private readonly IDiscordService _discordService;
    private readonly IConfiguration _configuration;

    public CompetitionController(ICompetitionService competitionService, IDiscordService discordService, IConfiguration configuration)
    {
        _competitionService = competitionService;
        _discordService = discordService;
        _configuration = configuration;
    }

    [DiscordAuthRequired(F7ScRoles.Admin)]
    [HttpPost("competitions")]
    public IActionResult CreateCompetition(CompetitionViewModel viewModel)
    {
        var competition = _competitionService.UpsertCompetition(viewModel);
        viewModel.Id = competition.Id;
        return Ok(viewModel);
    }

    [HttpGet("competitions")]
    public IActionResult GetAllCompetitions()
    {
        return Ok(_competitionService.GetAllCompetitions()
            .Select(CompetitionViewModel.FromDbModel)
            .ToList());
    }

    [DiscordAuthRequired(F7ScRoles.Admin)]
    [HttpGet("competitions-with-submissions")]
    public IActionResult GetAllCompetitionsWithSubmissionCount()
    {
        return Ok(_competitionService.GetAllCompetitionsWithSubmissions()
            .Select(CompetitionWithSubmissionCountViewModel.FromDbModel)
            .ToList());
    }

    [DiscordAuthRequired(F7ScRoles.Admin)]
    [HttpGet("competitions/{id}")]
    public IActionResult GetCompetition(int id)
    {
        var competition = _competitionService.GetCompetitionWithSubmissions(id);
        if (competition == null)
        {
            return NotFound();
        }

        competition.Submissions.Sort((s1, s2) => s2.Score - s1.Score);
        
        var guildMembers = _discordService.GetGuildMembers(_configuration["DiscordGuildId"]);

        var viewModel = new CompetitionWithSubmissionsViewModel
        {
            Id = competition.Id,
            Name = competition.Name,
            Platforms = competition.Platforms.Select(x => x.Platform).ToList(),
            Submissions = competition.Submissions.Select(submission => new SubmissionViewModel
            {
                Platform = submission.Platform.Platform,
                DiscordName =
                    $"{guildMembers[submission.DiscordId].User.Username}#{guildMembers[submission.DiscordId].User.Discriminator}",
                DurationInSeconds = submission.DurationInSeconds,
                Id = submission.Id,
                Score = submission.Score,
                Status = Enum.GetName(typeof(SubmissionStatus), submission.Status)!,
                Fingerprint = submission.Fingerprint,
                VideoUrls = submission.Videos.Select(video => video.VideoUrl).ToList()
            }).ToList()
        };
        
        return Ok(viewModel);
    }
}