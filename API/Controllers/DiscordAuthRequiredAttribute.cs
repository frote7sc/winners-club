using F7SCAPI.BusinessLogic.Discord;
using Microsoft.AspNetCore.Mvc.Filters;

namespace F7SCAPI.Controllers;

[AttributeUsage(AttributeTargets.Method, AllowMultiple = true)]
public class DiscordAuthRequiredAttribute : ActionFilterAttribute
{
    private readonly List<string> _roleIds = new();

    public DiscordAuthRequiredAttribute(string roleIds = "")
    {
        if (!string.IsNullOrWhiteSpace(roleIds))
        {
            _roleIds = roleIds.Split(",").ToList();            
        }
    }

    public override async Task OnActionExecutionAsync(ActionExecutingContext actionContext, ActionExecutionDelegate next)
    {
        try
        {
            // 1. Do we have the header?
            if (!actionContext.HttpContext.Request.Headers.TryGetValue("x-discord-token", out var headerValue) ||
                string.IsNullOrWhiteSpace(headerValue.ToString()))
            {
                actionContext.Result = new ResultWithBody(StatusCodes.Status401Unauthorized,
                    "A X-Discord-Token is required for this request.");
                return;
            }

            // 2. Is the token properly formed?
            var splitDiscordToken = headerValue.ToString().Split(' ');
            if (splitDiscordToken.Length != 2)
            {
                actionContext.Result = new ResultWithBody(StatusCodes.Status400BadRequest,
                    "The X-Discord-Token is malformed.");
                return;
            }

            // 3. Is the token still valid?
            var discordService = actionContext.HttpContext.RequestServices.GetService<IDiscordService>()!;
            var token = await discordService.VerifyTokenAsync(splitDiscordToken[0], splitDiscordToken[1]);
            if (token == null)
            {
                actionContext.Result = new ResultWithBody(StatusCodes.Status401Unauthorized, 
                    "The X-Discord-Token is invalid.");
                return;
            }

            // 4. Does the user have the necessary role(s)?
            var configuration = actionContext.HttpContext.RequestServices.GetService<IConfiguration>()!;
            if (await discordService.DoesUserHaveRolesAsync(configuration["DiscordGuildId"], token, _roleIds))
            {
                return;
            }
                
            actionContext.Result = new ResultWithBody(StatusCodes.Status403Forbidden,
                "User does not have access to this endpoint.");
        }
        finally
        {
            await base.OnActionExecutionAsync(actionContext, next);
        }
    }
}