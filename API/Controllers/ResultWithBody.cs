using Microsoft.AspNetCore.Mvc;

namespace F7SCAPI.Controllers;

public class ResultWithBody : JsonResult
{
    public ResultWithBody(int statusCode, string message) : base(new { message })
    {
        StatusCode = statusCode;
    }
}