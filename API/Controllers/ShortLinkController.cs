using DataAccess.Data;
using DataAccess.Models;
using F7SCAPI.BusinessLogic.Discord;
using F7SCAPI.ViewModels.ShortLinks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace F7SCAPI.Controllers;

public class ShortLinkController : ApiControllerBase
{
    private readonly F7ScDbContext _dbContext;

    public ShortLinkController(F7ScDbContext dbContext)
    {
        _dbContext = dbContext;
    }

    [HttpGet("short-links")]
    [DiscordAuthRequired(F7ScRoles.Admin)]
    public IActionResult GetShortLinks()
    {
        return Ok(_dbContext.ShortLinks.AsNoTracking().ToList());
    }
    
    [HttpGet("short-links/{key}")]
    public async Task<IActionResult> GetShortLink(string key)
    {
        var shortLink = _dbContext.ShortLinks.AsNoTracking().FirstOrDefault(shortLink => shortLink.Key == key);
        if (shortLink == null)
        {
            return NotFound();
        }

        shortLink.Uses++;
        _dbContext.Update(shortLink);
        await _dbContext.SaveChangesAsync();
        
        return Ok(shortLink);
    }

    [HttpPost("short-links")]
    [DiscordAuthRequired(F7ScRoles.Admin)]
    public async Task<IActionResult> CreateShortLink(CreateShortLinkRequestModel requestModel)
    {
        var shortLink = new ShortLink
        {
            Key = requestModel.Key,
            RedirectUrl = requestModel.Url
        };
        _dbContext.Update(shortLink);
        await _dbContext.SaveChangesAsync();

        return Ok(shortLink);
    }

    [HttpDelete("short-links/{id:int}")]
    [DiscordAuthRequired(F7ScRoles.Admin)]
    public async Task<IActionResult> DeleteShortLink(int id)
    {
        var shortLink = _dbContext.ShortLinks.FirstOrDefault(shortLink => shortLink.Id == id);
        if (shortLink == null)
        {
            return BadRequest();
        }

        _dbContext.Remove(shortLink);
        await _dbContext.SaveChangesAsync();

        return NoContent();
    }
}