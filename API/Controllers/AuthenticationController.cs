using Microsoft.AspNetCore.Mvc;

namespace F7SCAPI.Controllers;

public class AuthenticationController : ApiControllerBase
{
    [DiscordAuthRequired]
    [HttpGet("verify-discord")]
    public IActionResult CheckDiscordAuthentication()
    {
        //-- No-op here, as the DiscordAuthRequired attribute handles verifying the token
        return NoContent();
    }
}