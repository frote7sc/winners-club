using System.Globalization;
using DataAccess.Models.Competitions;
using F7SCAPI.BusinessLogic;

namespace F7SCAPI.ViewModels.Competitions;

public class CompetitionWithSubmissionCountViewModel : CompetitionViewModel
{
    public int NumberOfSubmissions { get; set; }
    public int NumberOfPendingSubmissions { get; set; }

    public new static CompetitionWithSubmissionCountViewModel FromDbModel(Competition competition)
    {
        return new CompetitionWithSubmissionCountViewModel
        {
            Id = competition.Id,
            Name = competition.Name,
            Description = competition.Description,
            StartTime = competition.StartTime.ToString("yyyy-MM-ddTHH:mm:ss'Z'", CultureInfo.InvariantCulture),
            EndTime = competition.EndTime.ToString("yyyy-MM-ddTHH:mm:ss'Z'", CultureInfo.InvariantCulture),
            Platforms = competition.Platforms.Select(x => x.Platform).ToList(),
            NumberOfPendingSubmissions = competition.Submissions.Count(x => x.Status == SubmissionStatus.Pending),
            NumberOfSubmissions = competition.Submissions.Count
        };
    }
}