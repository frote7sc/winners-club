using DataAccess.Models.Competitions;
using F7SCAPI.ViewModels.Submissions;

namespace F7SCAPI.ViewModels.Competitions;

public class CompetitionWithSubmissionsViewModel
{
    public int Id { get; set; }
    public string Name { get; set; }
    public List<string> Platforms { get; set; }
    public List<SubmissionViewModel> Submissions { get; set; }
}