using System.Globalization;
using DataAccess.Models.Competitions;

namespace F7SCAPI.ViewModels.Competitions;

public class CompetitionViewModel
{
    public int? Id { get; set; }
    public string Name { get; set; } = null!;
    public string? Description { get; set; }
    public string StartTime { get; set; } = null!;
    public string EndTime { get; set; } = null!;
    public List<string> Platforms { get; set; } = new();

    public static CompetitionViewModel FromDbModel(Competition competition)
    {
        return new CompetitionViewModel
        {
            Id = competition.Id,
            Name = competition.Name,
            Description = competition.Description,
            StartTime = competition.StartTime.ToString("yyyy-MM-ddTHH:mm:ss'Z'", CultureInfo.InvariantCulture),
            EndTime = competition.EndTime.ToString("yyyy-MM-ddTHH:mm:ss'Z'", CultureInfo.InvariantCulture),
            Platforms = competition.Platforms.Select(x => x.Platform).ToList()
        };
    }
}