namespace F7SCAPI.ViewModels;

public class PlatformToPublicIdViewModel
{
    public string Platform { get; set; }
    public string PublicId { get; set; }
}