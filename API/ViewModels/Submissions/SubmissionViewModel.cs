namespace F7SCAPI.ViewModels.Submissions;

public class SubmissionViewModel
{
    public int Id { get; set; }
    public string Platform { get; set; }
    public string DiscordName { get; set; } = null!;
    public int DurationInSeconds { get; set; }
    public int Score { get; set; }
    public string Status { get; set; } = null!;
    public List<string> VideoUrls { get; set; } = new();
    public Guid Fingerprint { get; set; }
}