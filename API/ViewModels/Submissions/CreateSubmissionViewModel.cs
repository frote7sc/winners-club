namespace F7SCAPI.ViewModels.Submissions;

public class CreateSubmissionViewModel
{
    public string Platform { get; set; }
    public int DurationInSeconds { get; set; }
    public int Score { get; set; }
    public List<string> VideoUrls { get; set; } = new();
}