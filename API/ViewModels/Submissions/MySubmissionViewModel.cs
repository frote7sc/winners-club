namespace F7SCAPI.ViewModels.Submissions;

public class MySubmissionViewModel
{
    public int Id { get; set; }
    public int CompetitionId { get; set; }
    public string CompetitionName { get; set; }
    public string Platform { get; set; }
    public int DurationInSeconds { get; set; }
    public int Score { get; set; }
    public string Status { get; set; }
    public List<string> VideoUrls { get; set; }
}