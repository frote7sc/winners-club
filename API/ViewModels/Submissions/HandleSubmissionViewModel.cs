namespace F7SCAPI.ViewModels.Submissions;

public class HandleSubmissionViewModel
{
    public Guid Fingerprint { get; set; }
    public string NewStatus { get; set; }
}