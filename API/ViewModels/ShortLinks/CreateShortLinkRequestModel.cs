namespace F7SCAPI.ViewModels.ShortLinks;

public class CreateShortLinkRequestModel
{
    public string Key { get; set; } = null!;
    public string Url { get; set; } = null!;
}