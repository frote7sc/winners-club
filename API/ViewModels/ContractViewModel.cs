namespace F7SCAPI.ViewModels;

public class ContractViewModel
{
    public string Event { get; set; }
    public string Winner { get; set; }
    public List<PlatformToPublicIdViewModel> ContractIds { get; set; }
}