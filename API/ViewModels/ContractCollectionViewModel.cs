namespace F7SCAPI.ViewModels;

public class ContractCollectionViewModel
{
    public int Month { get; set; }
    public int Year { get; set; }
    public List<ContractViewModel> Contracts { get; set; }
}