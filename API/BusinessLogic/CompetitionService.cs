using DataAccess.Data;
using DataAccess.Models.Competitions;
using F7SCAPI.ViewModels.Competitions;
using Microsoft.EntityFrameworkCore;

namespace F7SCAPI.BusinessLogic;

public class CompetitionService : ICompetitionService
{
    private readonly F7ScDbContext _dbContext;

    public CompetitionService(F7ScDbContext dbContext)
    {
        _dbContext = dbContext;
    }

    public Competition UpsertCompetition(CompetitionViewModel viewModel)
    {
        var platforms = _dbContext.CompetitionPlatforms
            .Where(x => viewModel.Platforms.Contains(x.Platform))
            .ToList();
        
        // ReSharper disable once ForeachCanBePartlyConvertedToQueryUsingAnotherGetEnumerator
        foreach (var platformString in viewModel.Platforms)
        {
            if (platforms.Any(x => string.Equals(x.Platform, platformString, StringComparison.InvariantCultureIgnoreCase)))
            {
                continue;
            }
            
            var newPlatform = new CompetitionPlatform
            {
                Platform = platformString
            };
            platforms.Add(newPlatform);
            _dbContext.Update(newPlatform);
        }

        Competition? competition = null;
        if (viewModel.Id.HasValue)
        {
            competition = _dbContext.Competitions
                .Include(x => x.Platforms)
                .FirstOrDefault(x => x.Id == viewModel.Id);
        }

        competition ??= new Competition();

        competition.Name = viewModel.Name;
        competition.Description = viewModel.Description;
        competition.StartTime = DateTime.Parse(viewModel.StartTime).ToUniversalTime();
        competition.EndTime = DateTime.Parse(viewModel.EndTime).ToUniversalTime();
        competition.Platforms = platforms;
        
        _dbContext.Update(competition);
        _dbContext.SaveChanges();

        return competition;
    }

    public Competition? GetCompetitionWithSubmissions(int id)
    {
        return _dbContext.Competitions
            .Include(x => x.Platforms)
            .Include(x => x.Submissions)
            .ThenInclude(y => y.Platform)
            .Include(x => x.Submissions)
            .ThenInclude(y => y.Videos)
            .AsNoTracking()
            .FirstOrDefault(x => x.Id == id);
    }

    public List<Competition> GetAllCompetitions()
    {
        return _dbContext.Competitions
            .Include(x => x.Platforms)
            .ToList();
    }

    public List<Competition> GetAllCompetitionsWithSubmissions()
    {
        return _dbContext.Competitions
            .Include(x => x.Platforms)
            .Include(x => x.Submissions)
            .OrderByDescending(x => x.EndTime)
            .ToList();
    }
}