namespace F7SCAPI.BusinessLogic.Discord;

public class GuildMembers
{
    public DateTime AccessedTime { get; }
    public Dictionary<string, GuildMember> Members { get; }

    public GuildMembers(DateTime accessedTime, Dictionary<string, GuildMember> members)
    {
        AccessedTime = accessedTime;
        Members = members;
    }

    public bool IsExpired()
    {
        return AccessedTime.AddMinutes(1) < DateTime.UtcNow;
    }
}