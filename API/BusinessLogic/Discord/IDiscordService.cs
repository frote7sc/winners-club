namespace F7SCAPI.BusinessLogic.Discord;

public interface IDiscordService
{
    Task<string?> VerifyTokenAsync(string tokenType, string token);

    Dictionary<string, GuildMember> GetGuildMembers(string guildId);

    Task<bool> DoesUserHaveRolesAsync(string guildId, string userId, List<string> roleIds);
}