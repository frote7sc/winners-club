namespace F7SCAPI.BusinessLogic.Discord;

public class GuildMember
{
    public User User { get; set; } = null!;
    public List<string> Roles { get; set; } = new();
}