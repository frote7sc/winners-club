using System.Net;
using System.Net.Http.Headers;
using System.Text.Json;

namespace F7SCAPI.BusinessLogic.Discord;

public class DiscordService : IDiscordService
{
    private readonly HttpClientWrapper _httpClientWrapper;
    private readonly IConfiguration _configuration;
    private readonly object syncLock = new object();

    private Dictionary<string, GuildMembers> _guildToGuildMembers = new();

    public DiscordService(HttpClientWrapper httpClientWrapper, IConfiguration configuration)
    {
        _httpClientWrapper = httpClientWrapper;
        _configuration = configuration;
    }

    public async Task<string?> VerifyTokenAsync(string tokenType, string token)
    {
        using var request = new HttpRequestMessage(HttpMethod.Get,
            "https://discordapp.com/api/v9/users/@me");
        request.Headers.Authorization = new AuthenticationHeaderValue(tokenType, token);
        var response = await _httpClientWrapper.GetClient().SendAsync(request);

        if (response.StatusCode != HttpStatusCode.OK)
        {
            return null;
        }

        return JsonSerializer.Deserialize<User>(await response.Content.ReadAsStreamAsync(),
            new JsonSerializerOptions
            {
                PropertyNameCaseInsensitive = true
            })?.Id;
    }

    public Dictionary<string, GuildMember> GetGuildMembers(string guildId)
    {
        lock (syncLock)
        {
            if (_guildToGuildMembers.ContainsKey(guildId) && !_guildToGuildMembers[guildId].IsExpired())
            {
                return _guildToGuildMembers[guildId].Members;
            }

            _guildToGuildMembers.Remove(guildId);

            var guildMembers = new Dictionary<string, GuildMember>();
            bool keepRequesting;
            var lastSnowflake = 0L;
            do
            {
                using var request = new HttpRequestMessage(HttpMethod.Get,
                    $"https://discordapp.com/api/v9/guilds/{guildId}/members?limit=1000&after={lastSnowflake}");
                request.Headers.Authorization = new AuthenticationHeaderValue("Bot", _configuration["DiscordBotToken"]);
                var response = _httpClientWrapper.GetClient().Send(request);
                response.EnsureSuccessStatusCode();
                var members = JsonSerializer.Deserialize<List<GuildMember>>(response.Content.ReadAsStream(),
                    new JsonSerializerOptions
                    {
                        PropertyNameCaseInsensitive = true
                    })!;
                members.ForEach(x =>
                {
                    if (long.Parse(x.User.Id) > lastSnowflake)
                    {
                        lastSnowflake = long.Parse(x.User.Id);
                    }
                    guildMembers[x.User.Id] = x;
                });

                keepRequesting = members.Count == 1000;
            } while (keepRequesting);

            _guildToGuildMembers[guildId] = new GuildMembers(DateTime.UtcNow, guildMembers);

            return guildMembers;
        }
    }

    public async Task<bool> DoesUserHaveRolesAsync(string guildId, string userId, List<string> roleIds)
    {
        using var request = new HttpRequestMessage(HttpMethod.Get,
            $"https://discordapp.com/api/v9/guilds/{guildId}/members/{userId}");
        request.Headers.Authorization = new AuthenticationHeaderValue("Bot", _configuration["DiscordBotToken"]);
        var response = await _httpClientWrapper.GetClient().SendAsync(request);

        try
        {
            response.EnsureSuccessStatusCode();
        }
        catch (HttpRequestException)
        {
            return false;
        }

        var guildMember = JsonSerializer.Deserialize<GuildMember>(await response.Content.ReadAsStreamAsync(),
            new JsonSerializerOptions
            {
                PropertyNameCaseInsensitive = true
            });

        return guildMember != null && roleIds.All(guildMember.Roles.Contains);
    }
}