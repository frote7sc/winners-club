using DataAccess.Models;
using F7SCAPI.ViewModels;

namespace F7SCAPI.BusinessLogic;

public interface IWinnersClubService
{
    void UpsertWinnersClubBatch(ContractCollectionViewModel viewModel);

    ContractCollection? GetWinnersClubBatch(int month, int year);
}