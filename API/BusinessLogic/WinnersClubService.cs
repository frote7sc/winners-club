using DataAccess.Data;
using DataAccess.Models;
using F7SCAPI.ViewModels;
using Microsoft.EntityFrameworkCore;

namespace F7SCAPI.BusinessLogic;

public class WinnersClubService : IWinnersClubService
{
    private readonly F7ScDbContext _dbContext;

    public WinnersClubService(F7ScDbContext dbContext)
    {
        _dbContext = dbContext;
    }

    public void UpsertWinnersClubBatch(ContractCollectionViewModel viewModel)
    {
        var contractCollection =
            _dbContext.ContractCollections
                .Include(x => x.Contracts)
                .ThenInclude(x => x.ContractIds)
                .FirstOrDefault(x =>
                    x.Month == viewModel.Month && x.Year == viewModel.Year) ?? new ContractCollection();

        contractCollection.Month = viewModel.Month;
        contractCollection.Year = viewModel.Year;
        contractCollection.Contracts = viewModel.Contracts.Select(contract => new Contract
        {
            Event = contract.Event,
            Winner = contract.Winner,
            ContractIds = contract.ContractIds.Select(id => new PlatformToPublicId
            {
                Platform = id.Platform,
                PublicId = id.PublicId
            }).ToList()
        }).ToList();

        _dbContext.Update(contractCollection);
        _dbContext.SaveChanges();
    }

    public ContractCollection? GetWinnersClubBatch(int month, int year)
    {
        return _dbContext.ContractCollections
            .Include(x => x.Contracts)
            .ThenInclude(x => x.ContractIds)
            .FirstOrDefault(x => x.Month == month && x.Year == year);
    }
}