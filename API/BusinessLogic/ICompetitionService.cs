using DataAccess.Models.Competitions;
using F7SCAPI.ViewModels.Competitions;

namespace F7SCAPI.BusinessLogic;

public interface ICompetitionService
{
    Competition UpsertCompetition(CompetitionViewModel viewModel);

    Competition? GetCompetitionWithSubmissions(int id);

    List<Competition> GetAllCompetitions();

    List<Competition> GetAllCompetitionsWithSubmissions();
}