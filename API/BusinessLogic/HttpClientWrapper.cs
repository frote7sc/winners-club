namespace F7SCAPI.BusinessLogic;

public class HttpClientWrapper
{
    private static readonly HttpClient HttpClient;

    static HttpClientWrapper()
    {
        HttpClient = new HttpClient();
    }

    public HttpClient GetClient()
    {
        return HttpClient;
    }
}