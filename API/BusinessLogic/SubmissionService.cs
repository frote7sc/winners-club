using DataAccess.Data;
using DataAccess.Models.Competitions;
using F7SCAPI.BusinessLogic.Discord;
using F7SCAPI.ViewModels.Competitions;
using F7SCAPI.ViewModels.Submissions;
using Microsoft.EntityFrameworkCore;

namespace F7SCAPI.BusinessLogic;

public class SubmissionService : ISubmissionService
{
    private readonly F7ScDbContext _dbContext;

    public SubmissionService(F7ScDbContext dbContext)
    {
        _dbContext = dbContext;
    }

    public CompetitionSubmission Submit(string discordId, int competitionId, CreateSubmissionViewModel viewModel)
    {
        var competition = _dbContext.Competitions.FirstOrDefault(x => x.Id == competitionId);
        if (competition == null || competition.EndTime < DateTime.UtcNow)
        {
            throw new Exception("Competition not found, or submissions have closed.");
        }
        
        // Do we have a previous submission?
        var submission = _dbContext.Submissions
            .Include(x => x.Platform)
            .Include(x => x.Videos)
            .FirstOrDefault(x => x.Competition == competition && x.DiscordId == discordId);
        if (submission == null)
        {
            // First submission
            submission = new CompetitionSubmission
            {
                Competition = competition,
                DiscordId = discordId
            };
        }

        submission.Platform = _dbContext.CompetitionPlatforms.First(x => x.Platform == viewModel.Platform);
        // Even if previously approved, always reset to pending
        submission.Status = SubmissionStatus.Pending;
        submission.Score = viewModel.Score;
        submission.DurationInSeconds = viewModel.DurationInSeconds;
        submission.Fingerprint = new Guid();
        submission.Videos = viewModel.VideoUrls.Select(x => new CompetitionSubmissionVideo
        {
            VideoUrl = x
        }).ToList();

        _dbContext.Update(submission);
        _dbContext.SaveChanges();
        
        return submission;
    }

    public Guid HandleSubmission(int competitionId, int submissionId, HandleSubmissionViewModel viewModel)
    {
        var competition = _dbContext.Competitions.FirstOrDefault(x => x.Id == competitionId);
        if (competition == null)
        {
            throw new Exception($"Could not find competition with ID {competitionId}");
        }

        var submission = _dbContext.Submissions.FirstOrDefault(x => x.Id == submissionId && 
                                                                    x.Competition == competition);
        if (submission == null)
        {
            throw new Exception($"Could not find submission with ID {submissionId}");
        }

        if (submission.Fingerprint != viewModel.Fingerprint)
        {
            throw new FingerprintViolationException();
        }

        submission.Status = Enum.Parse<SubmissionStatus>(viewModel.NewStatus);
        submission.Fingerprint = Guid.NewGuid();
        _dbContext.Update(submission);
        _dbContext.SaveChanges();

        return submission.Fingerprint;
    }

    public List<CompetitionSubmission> GetMySubmissions(string discordId)
    {
        return _dbContext.Submissions
            .Include(x => x.Competition)
            .Include(x => x.Platform)
            .Include(x => x.Videos)
            .Where(x => x.DiscordId == discordId).ToList();
    }
}