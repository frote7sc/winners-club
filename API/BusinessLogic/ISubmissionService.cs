using DataAccess.Models.Competitions;
using F7SCAPI.ViewModels.Competitions;
using F7SCAPI.ViewModels.Submissions;

namespace F7SCAPI.BusinessLogic;

public interface ISubmissionService
{
    public CompetitionSubmission Submit(string discordId, int competitionId, CreateSubmissionViewModel viewModel);

    public Guid HandleSubmission(int competitionId, int submissionId, HandleSubmissionViewModel viewModel);

    public List<CompetitionSubmission> GetMySubmissions(string discordId);
}